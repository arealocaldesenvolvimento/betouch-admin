<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Api\DashboardController;
use App\Http\Controllers\Api\ServicesController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\BookingController;
use App\Http\Controllers\Api\ContractController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Auth routes
 */
Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::get('refresh', [AuthController::class, 'refresh']);
});

/**
 * Authenticated routes
 */
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('edit/{id}', [AuthController::class, 'edit']);

    Route::get('user', [AuthController::class, 'user']);
    Route::prefix('auth')->group(function () {
        Route::post('logout', [AuthController::class, 'logout']);
    });

    /**
     * Api resources
     */
    Route::prefix('dashboard')->group(function () {
        Route::get('data', [DashboardController::class, 'fetchData']);
    });

    Route::apiResources([
        'services' => ServicesController::class,
        'users' => UsersController::class,
        'booking' => BookingController::class,
        'contract' => ContractController::class,
    ]);
});
