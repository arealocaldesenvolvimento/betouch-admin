import axios from "axios";

export default {
    namespaced: true,

    state: {
        alert: {
            error: false,
            succes: false,
            message: "",
            color: "green"
        },

        data: {
            agendamentos: 0,
            professionals: 0,
            clients: 0,
            range: []
        }
    },

    getters: {
        data: state => {
            return state.data;
        },

        range: state => {
            return state.data.range;
        }
    },

    mutations: {
        MUTATE_ALERT(state, payload) {
            const { type, message, color } = payload;

            if (type === "success") {
                state.alert.success = true;
            } else if (type === "error") {
                state.alert.error = true;
            }
            state.alert.color = color;
            state.alert.message = message;
        },

        MUTATE_DATA(state, { users, agendamentos, range }, { ...payload }) {
            let pros = 0;
            let cli = 0;

            users.map(user => {
                const { tipo } = user;
                if (tipo === "prestador") {
                    pros++;
                } else {
                    cli++;
                }
            });
            state.data.professionals = pros;
            state.data.clients = cli;
            state.data.agendamentos = agendamentos.length;
            state.data.range = range;
        }
    },

    actions: {
        SET_ALERT({ commit }, payload) {
            commit("MUTATE_ALERT", payload);
        },

        async FETCH_DATA({ commit }) {
            const response = await axios.get("/dashboard/data");
            commit("MUTATE_DATA", response.data);
        }
    }
};
