import axios from "axios";

export default {
    namespaced: true,
    state: {
        users: []
    },
    getters: {
        users: state => {
            return state.users;
        }
    },
    mutations: {
        MUTATE_USERS(state, payload) {
            state.users = payload;
        }
    },
    actions: {
        async FETCH_USERS({ commit }) {
            const response = await axios.get("users");
            commit("MUTATE_USERS", response.data);
        }
    }
};
