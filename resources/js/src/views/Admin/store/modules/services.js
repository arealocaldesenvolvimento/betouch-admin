import axios from "axios";

export default {
    namespaced: true,
    state: {
        services: []
    },
    getters: {
        services: state => {
            return state.services;
        }
    },

    mutations: {
        MUTATE_SERVICES(state, { services }) {
            state.services = services;
        }
    },

    actions: {
        async FETCH_SERVICES({ commit }) {
            const response = await axios.get("/services");
            commit("MUTATE_SERVICES", response.data);
        }
    }
};
