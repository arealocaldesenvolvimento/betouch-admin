import axios from "axios";

export default {
    namespaced: true,
    state: {
        bookings: []
    },
    getters: {
        bookings: state => {
            return state.bookings;
        }
    },
    mutations: {
        MUTATE_BOOKINGS(state, payload) {
            state.bookings = payload;
        }
    },
    actions: {
        async FETCH_BOOKINGS({ commit }, payload) {
            const response = await axios.get("booking");
            commit("MUTATE_BOOKINGS", response.data);
        }
    }
};
