import axios from "axios";
export default {
    namespaced: true,
    state: {
        contracts: []
    },
    getters: {
        contracts: state => {
            return state.contracts;
        }
    },
    mutations: {
        MUTATE_CONTRACTS(state, payload) {
            state.contracts = payload;
        }
    },
    actions: {
        async FETCH_CONTRACTS({ commit }, payload) {
            const response = await axios.get("contract");
            commit("MUTATE_CONTRACTS", response.data);
        }
    }
};
