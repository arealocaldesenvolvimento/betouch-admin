import AdminModule from "./modules/dashboard";
import ServiceModule from "./modules/services";
import UsersModule from "./modules/users";
import BookingModule from "./modules/booking";
import ContractModule from "./modules/contracts";

export {
    AdminModule,
    ServiceModule,
    UsersModule,
    BookingModule,
    ContractModule
};
