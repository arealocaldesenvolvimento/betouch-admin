import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Login from "../views/Auth/login.vue";
import Dashboard from "../views/Admin/dashboard";
import Servicos from "../views/Admin/servicos";
import Agendamento from "../views/Admin/agendamento";
import Termos from "../views/Admin/termos";
import Pagamentos from "../views/Admin/pagamentos";
import Pessoas from "../views/Admin/pessoas";

const routes = [
    {
        path: "/",
        name: "dashboard",
        component: Dashboard,
        meta: {
            auth: true
        }
        // children: [
        //     {
        //         // UserProfile will be rendered inside User's <router-view>
        //         // when /user/:id/profile is matched
        //         path: "/servicos",
        //         component: Servicos
        //     },
        //     {
        //         // UserPosts will be rendered inside User's <router-view>
        //         // when /user/:id/posts is matched
        //         path: "posts",
        //         component: UserPosts
        //     }
        // ]
    },
    {
        path: "/pessoas",
        name: "pessoas",
        component: Pessoas,
        meta: {
            auth: true
        }
    },
    {
        path: "/servicos",
        name: "servicos",
        component: Servicos,
        meta: {
            auth: true
        }
    },
    {
        path: "/agendamentos",
        name: "agendamentos",
        component: Agendamento,
        meta: {
            auth: true
        }
    },
    {
        path: "/termos",
        name: "termos",
        component: Termos,
        meta: {
            auth: true
        }
    },
    {
        path: "/pagamentos",
        name: "pagamentos",
        component: Pagamentos,
        meta: {
            auth: true
        }
    },
    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            auth: false
        }
    }
];

const router = new VueRouter({
    history: true,
    mode: "history",
    routes
});

Vue.router = router;

export default router;
