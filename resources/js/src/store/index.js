import Vue from "vue";
import Vuex from "vuex";

import AuthModule from "../views/Auth/store";
import {
    AdminModule,
    ServiceModule,
    UsersModule,
    BookingModule,
    ContractModule
} from "../views/Admin/store";

Vue.use(Vuex);

const modules = {
    AuthModule,
    AdminModule,
    ServiceModule,
    UsersModule,
    BookingModule,
    ContractModule
};

export default new Vuex.Store({
    modules
});
