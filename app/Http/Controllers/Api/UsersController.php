<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ServicoUsers;
use App\Models\Servico;
use App\Models\Users;
use Illuminate\Support\Arr;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::all();
        foreach ($users as $user ) {
            $services = ServicoUsers::select(
                'servico.nome as servico'
            )
            ->join('users','users.id','=','servico_users.users_id')
            ->join('servico','servico_users.servico_id', '=', 'servico.id')
            ->where('servico_users.users_id', '=', $user->id)
            ->groupBy('servico.nome')
            ->get();
            $services_user = [];            
            foreach ($services as $service) {
                array_push($services_user, $service->servico);
            }
            $user->service = $services_user;
        }

        return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if  ($user->destroy()) {
            return response()->json($user, 200);
        }

        return response()->json('erro', 422);
    }
}
