<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Agendamento;
use App\Models\Users;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //

    public function fetchData()
    {
        $agendamentos = Agendamento::where('status', '=', 'finalizado')->get();
        $users = Users::all();

        $data = collect(range(11, 0));

        $range = $data->map(function ($i) {
            $dt = today()->startOfMonth()->subMonth($i);
            $bookingsMonth = Agendamento::date($dt->month)->count();
            return [
            'month' => $dt->shortMonthName,
            'year' => $dt->format('Y'),
            'bookings' => $bookingsMonth
            ];
        });

        return response()->json([
            'range' => $range,
            'users' => $users,
            'agendamentos' => $agendamentos
        ], 200);
    }
}
