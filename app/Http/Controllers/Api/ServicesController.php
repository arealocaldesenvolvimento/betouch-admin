<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Servico;

class ServicesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $services = Servico::all();
        foreach ($services as $service) {
            $newData = [
                'id' => $service->id,
                'nome' => $service->nome,
                'categoria' => $service->category->nome
            ];
            array_push($data, $newData);
        }

        return response()->json(['services' => $data], 200);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Servico::find($id);
        $service->nome = $request->name;
        $service->save();
        
        return response()->json($service, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servico = Servico::destroy($id);
        return response()->json($servico, 200);
    }
}
