<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    use HasFactory;

    protected $table = 'servico';
    protected $fillable = [
        'nome',
        'categoria_servico_id'
    ];

    /**
     * Get the category for the service
     */
    public function category()
    {
        return $this->belongsTo('App\Models\CategoriaServico', 'categoria_servico_id');
    }
}
