<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicoUsers extends Model
{
    use HasFactory;

    protected $table = 'servico_users';

    protected $fillable = [
        'dia_semana',
        'hora_inicio',
        'hora_termino',
        'duracao_servico',
        'domicilio',
        'valor',
        'users_id',
        'servico_id',
        'ativo',
        'descricao'
    ];
}
