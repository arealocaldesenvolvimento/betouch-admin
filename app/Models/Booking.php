<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'agendamento';

    protected $fillable = [
        'inicio',
        'fim',
        'valor',
        'status',
        'data',
        'endereco_id',
        'agenda_id',
        'descricao',
        'cliente_id',
        'prestador_id',
        'servico_id',
        'event_id'
    ];
}
