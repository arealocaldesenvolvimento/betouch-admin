<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriaServico extends Model
{
    use HasFactory;

    protected $table = 'categoria_servico';
    protected $fillable = [
        'nome'
    ];

    /**
     * Get the services for the category.
     */
    public function services()
    {
        return $this->hasMany('App\Models\Servico');
    }
}
