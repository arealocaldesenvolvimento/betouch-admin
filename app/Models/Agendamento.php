<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agendamento extends Model
{
    use HasFactory;

    protected $table = 'agendamento';

    protected $fillable = [
        'inicio',
        'fim',
        'valor',
        'status',
        'data',
        'endereco_id',
        'agenda_id',
        'descricao',
        'cliente_id',
        'prestador_id',
        'servico_id',
        'event_id'
    ];

    public function scopeDate($query, $month)
    {
        return $query->whereMonth('created_at', $month);
    }
}
