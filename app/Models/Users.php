<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = [
        'id',
        'name',
        'email',
        'email_verified_at',
        'password',
        'password',
        'remember_token',
        'cpfcnpj',
        'rgie',
        'razaosocial',
        'nascimento',
        'telefonecelular',
        'sexo',
        'tipo',
        'provider_id',
        'avatar',
        'rating'
    ];
}
